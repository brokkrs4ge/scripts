#!/bin/sh
# get directory of project

curDIR=$(pwd)
read -p "Path to initialize go project (${curDIR}): " curDIR;
if [ -z "${curDIR}" ];then
	curDIR=$(pwd)
	echo ''
fi


# check if go.mod already exists
if [ -f "go.mod" ]; then
    	RED='\033[0;31m'
	NC='\033[0m' # No Color
	echo "${RED}go.mod${NC} file already exists in ${RED}$(pwd)${NC}. Aborting...${NC}"

	exit
fi
# get the namespace
read -p 'project-namespace(mandatory): ' namespace
while [ -z "$namespace" ]; do
	read -p 'project-namespace cannot be empty: ' namespace
done

# get description for readme
read -p 'description: ' description

DOCFOLDER='docs'

read -p "documentation folder (docs): "
if [ ! -z "${REPLY}" ]
then
	DOCFOLDER=$REPLY
fi

read -p "create Makefile? [y/N]? " create_makefile


mkdir -p $curDIR;
pushd $curDIR;

if [[ -n $description ]]; then
	cat << EOT >> README
README
$desc
EOT
fi

BOOTSTRAP_ASSETS=https://gitlab.com/brokkrs4ge/scripts/-/raw/master/bootstrappers/go/api-server/assets
curl -o main.go $BOOTSTRAP_ASSETS/main.go
curl -o Dockerfile $BOOTSTRAP_ASSETS/Dockerfile
curl -o .dockerignore $BOOTSTRAP_ASSETS/.dockerignore
curl -o docker-compose.yml $BOOTSTRAP_ASSETS/docker-compose.yml

mkdir -p $DOCFOLDER domain model store/migrations


go mod init $namespace;
go mod tidy
#create Makefile
if [[ $create_makefile =~ ^[Yy]$ ]]
then
    cat <<EOT >> Makefile
PKG=$namespace
PKGLIST=\$(shell go list \${PKG}/... | grep -v /vendor/)

build: dep
	@go build -o main
test:
	@go test -short \${PKGLIST}

dep:
	@go get -v -d -t ./...

EOT

fi




#!/bin/bash

# installs nvm
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash

# installs node12
nvm install 12; nvm use 12
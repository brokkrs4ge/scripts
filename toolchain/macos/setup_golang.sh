#!/bin/bash


OS="darwin"
ARCH="amd64"
GOVER=go1.16.5

if [ "$#" -ne 1 ]; then
    GOVER="go1.16.5"
else
    GOVER="go${1}"
fi

TARBALL="${GOVER}.${OS}-${ARCH}.tar.gz"
dlPath="https://golang.org/dl/${TARBALL}"

curl -OL $dlPath
tar -C ./ -xvf $TARBALL
mv go/ "${GOVER}/" #rename
rm -rf $TARBALL
mv "${GOVER}" "/usr/local/${GOVER}"

#make symbolic link
# ln -s "/usr/local/go1.14.9" "/usr/local/go"

# create .bash_profile under $HOME
# setup
# export GOROOT=/usr/local/go
# export GOPATH=$HOME/Documents/go
# export PATH=$PATH:$GOROOT/bin:$GOPATH
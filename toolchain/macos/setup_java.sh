#!/bin/bash 

brew install openjdk@11


# for apple Silicon
# sudo ln -sfn /opt/homebrew/opt/openjdk@11/libexec/openjdk.jdk \
#      /Library/Java/JavaVirtualMachines/openjdk-11.jdk


# for apple intel
sudo ln -sfn /usr/local/opt/openjdk@11/libexec/openjdk.jdk /Library/Java/JavaVirtualMachines/openjdk-11.jdk

# to set java version,
# run export JAVA_HOME=`/usr/libexec/java_home -v 8`
#  and reload terminal